const io = require('socket.io-client');
const User = require('./core/user');

const smartbeSocket = function(url, sendMessage ){

    this.socket = io(url,
        {
            transportOptions: {
                polling: {
                    extraHeaders: {
                        'Cookie': 'client=smartbe.site;'
                    }
                }
            }
        }
    );

    let sendMessageFunc = sendMessage;

    this.socket.on('connect', function(){
        console.log("Connected to chat-api");
    });

    this.socket.on("smartbe.buyer", function(msg, fn){
        console.log(msg);

        if (!(msg.message !== undefined && msg.expert_id !== undefined) ) {
            console.log("Message or expert_id is empty");
            return;
        }
        
        let sender;

        if (msg.message.sender && msg.message.sender.name && msg.message.sender.type_id )
            sender = new User(msg.message.sender.type_id, msg.message.sender.name)
        else{
            sender = new User(User.type.expert, `expertId-${msg.expert_id}`);
        }


        sendMessageFunc(msg.message.content, sender );

        try {
            fn({ messageId : msg.message.message_id });
        }
        catch (e) {
            console.log('could not call the ack function'  + e);
        }
    });

    this.socket.on("smartbe.system", function(msg, fn){
        console.log(msg);

        let extSocket = require('socket.io-client').connect("http://localhost:8080");
        extSocket.on('connect', () => {
            console.log("External connected");

            extSocket.emit("system_message", msg);

            fn({dialog_id: msg.dialog_id,
                statusId: 3});

        })

    });

};


// Cookie: yummy_cookie=choco;


module.exports = smartbeSocket;