# cbp-test-client
Test client for ChatBotPlatform Api

# Installation

1. Install dependencies 

    ```
    npm install
    ```

2. Copy config_def.js to .config.js
    
   ```
   cp config_def.js .config.js
   ```
3. In .config.js set correct parameters

4. Run the chat's client back end
    ```
    node index.js
    ```
