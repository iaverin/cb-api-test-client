// some functions

function renderButtons(title, buttons){

    var resultHtml  = "<p>"+title+"</p>";

    for (let button of buttons ){
        if (button.userReply !== undefined) {
            resultHtml += '<input type = "button" value="' + button.title +
                '" onclick ="sendButton(\'' + button.payload + '\', \'' + button.userReply + '\')" style="margin-left:5px"/>';
        }
        else {
            resultHtml += '<input type = "button" value="' + button.title +
                '" onclick ="sendButton(\'' + button.payload + '\')" style="margin-left:5px"/>';
        }
        }
       
    return resultHtml

}

module.exports = {renderButtons}


