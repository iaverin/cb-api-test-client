const CONSOLE_USER = undefined;

/**
 * returns all the connected sockets for provided users
 * @param {*} sockets all sockets from io.sockets.sockets
 * @param  {...any} users 
 */
function userSockets(sockets, ...users) {

    let usersToSend;

    if (Array.isArray(users) && Array.isArray(users[0]) )
         usersToSend = users[0];
    else  usersToSend = users;

    let usersSockets = [];
    let userNames = [];

    for (let user of usersToSend){
        if (user !== undefined) userNames.push(user.name);
    } 

    for (let s_id in sockets){
        let s = sockets[s_id];

        if (s.user !== undefined && userNames.includes(s.user.name)) 
         {
            usersSockets.push(s);
        }
    }
    return usersSockets;
}


/**
 * send notification text to users 
 * @param {*} sockets all connected sockets
 * @param {*} text text
 * @param  {...any} users 
 */
function sendNotification(sockets, text, ...users){
    let usersToSend;

    if (Array.isArray(users) && Array.isArray(users[0]) )
        usersToSend = users[0];
    else  usersToSend = users;

    userSockets(sockets, usersToSend).forEach( (socket) =>
        {
           socket.emit("notification", text);
         }
      )
}


/**
 * send message to all connected  buyer and seller clients in dialog. If smartbeSocket is provided
 * - send also to expert chat (cb-ap)
 *
 * @param sockets all sockets
 * @param dialog
 * @param sender
 * @param text
 * @param smartbeSocket
 */
function sendToClientsInDialog(sockets, dialog, sender, text, smartbeSocket = undefined,
                               expertOnly=false){
    let usersToSend = [dialog.buyer, dialog.expert]

    if (expertOnly) {
        usersToSend = [dialog.expert]
    }

    userSockets(sockets, ...usersToSend).forEach(socket => {
        if (socket.dialog !== undefined && socket.dialog.id === dialog.id) 
            socket.emit("chat_message", `<b>${sender.name}</b>: ${text}`);});
    
    if (sender !== undefined && sender.role === "buyer" && dialog.state === "expert" 
           && dialog.expert !== undefined ){
            sendNotification(sockets,`[${dialog.id}]: new message`, dialog.expert);
    }

    if (dialog.state === "expert" && sender.role === "buyer" && smartbeSocket !== undefined )
        {
            smartbeSocket.socket.emit("smartbe.buyer", JSON.stringify({senderId: dialog.buyer.name, text: text}));
        }

}



/**
 * send notification when the dialog's state changed. If the expert is not assigned - send to all connected experts. 
 * @param {*} sockets 
 * @param {*} dialog 
 * @param {*} prevState 
 * @param {*} newState 
 */
function notificationDialogState(sockets, dialog, prevState, newState){

    dialog.postMessage(`Changed state of dialog ${prevState} -> ${newState}`, CONSOLE_USER, 
        (dialog, sender, message) => {sendToClientsInDialog(sockets, dialog, sender, message)});
    
    // if (dialog.state === "bot" ){

    // }
    
    // send notification to assigned experts
    if (dialog.expert !== undefined && dialog.state === "expert")
        {sendNotification(sockets,`New state of dialog ${prevState} -> ${newState}`, dialog.expert);
        return; }

    // send notification to all experts
    if(dialog.state === "expert" && dialog.expert === undefined){
        let allExperts=new Array();
        for (let s_id in sockets){
            let s = sockets[s_id];
   
            if (s.user !== undefined && s.user.role === "expert") 
                allExperts.push(s.user);
        }
        sendNotification(sockets, `New dialog: ${dialog.id}`, ...allExperts);

    }
}

async function parseResponses(sockets, dialog, responses, botUser, onSendTyping, smartbeSocket)
{
    try {
        for (let resp of responses) {

            if (resp.type === "text") {
                let text = decodeURIComponent(resp.text);

                dialog.postMessage(text, botUser, (dialog, sender, message) =>{
                    sendToClientsInDialog(sockets, dialog, sender, message)});
            }
            else if(resp.type === "buttons" ){
                dialog.postMessage(resp, botUser, (dialog, sender, message) =>{
                    sendToClientsInDialog(sockets, dialog, sender, message)});
            }
            else if(resp.type === "typing"){
              //  typing = true;
                dialog.postMessage({type: "typing"}, botUser, onSendTyping);
                // io.emit("typing", "");
                await delay(1000);
                //  typing = false
            }
            else if(resp.type === "system" ){
                //io.emit('chat_message', "SYSTEM MESSAGE");
                dialog.postMessage("System message: " + JSON.stringify(resp), botUser,
                    (dialog, sender, message) => {
                        sendToClientsInDialog(sockets, dialog, sender, message)}) ;

                if (resp.state !== undefined){
                    try{
                        dialog.changeState(resp.state,
                            (dialog, prevState, newState ) => {notificationDialogState(sockets,
                                dialog, prevState, newState)});
                    }
                    catch (e){
                        console.log("reject:" + e);
                    }
                }}

            else if (resp.type === "user_message"){

                dialog.postMessage(resp.text, dialog.buyer,
                    (dialog, sender, message) =>
                    {sendToClientsInDialog(sockets, dialog, sender, message, smartbeSocket)});
            }

            else if (resp.type === "expert_only"){

                // smartbeSocket.socket.emit("smartbe.buyer",
                //     JSON.stringify({senderId: dialog.buyer.name, text: resp.text}));

                dialog.postMessage(resp.text, dialog.buyer,
                    (dialog, sender, message) =>
                    {sendToClientsInDialog(sockets, dialog, sender, message, smartbeSocket,true )});


            }

            else {

                dialog.postMessage("Unknown message: " + JSON.stringify(resp), botUser,
                    (dialog, sender, message) =>{
                        sendToClientsInDialog(sockets, dialog, sender, message)}) ;
            }
        }


    }
    catch (e){
        console.log("Empty response or Could not parse response: " + e);
        userSockets(sockets, dialog.buyer).forEach (
            (socket) => {socket.emit('chat_message', '<strong>ERROR:</strong> ' + e)});
    }
}

const delay = (amount = number) => {
    return new Promise((resolve) => {
        setTimeout(resolve, amount);
    });
}



module.exports.userSockets = userSockets;
module.exports.sendNotification = sendNotification;
module.exports.notificationDialogState = notificationDialogState;
module.exports.sendToClientsInDialog = sendToClientsInDialog;
module.exports.parseResponses = parseResponses;
