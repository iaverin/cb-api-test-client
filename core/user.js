
const  type = {
        buyer : 0,
        seller : 1,
        bot :  2,
        expert : 3};

const User = function(typeId, name) {

    //    type : {
    //    buyer : 0,
    //    seller : 1,
    //    bot :  2,
    //    expert : 3},

    this.roleByType = {
           0:"buyer",
           1:"seller",
           2:"bot",
           3:"expert"
       },

    this.typeId = typeId,
    this.role = this.roleByType[typeId],
    this.name = name,

    this.typeFromString = function(role) {
            let typeId = eval ("type."+ role);
            if (typeId !== undefined)
                return typeId;
            else
                throw new Error("Wrong role");
    }

}

module.exports = User;
module.exports.type = type;
