
elements = require("./elements");

const Dialogs = function() {
    const list = new Object();

    const add = function (dialog) {
        if (this.list[dialog.is] === undefined)
            this.list[dialog.id] = dialog;

        else{

        }
    }
}

const Message =function (dialog,  sender, content, timestamp = undefined ) {
// INFO: Формат получения новых сообщений по подписке
// const FULL_MESSAGE_FORMAT = `{
//     message_id
//     dialog_id
//     timestamp
//     timestamp_received
//     is_read
//     is_delivered
//     content
//     sender {
//       sender_id
//       name
//       picture_path
//       type_id
//     }
//   }`;

    this.dialog = dialog;
    this.sender = sender;
    this.timestamp = timestamp ? undefined : Date.now() ;
    
    this.content = {};

    if (content.type === undefined){
        this.content.type = "text";
        this.content.text = content;
    }
    else {
        this.content = content; 
    }
    
}

const Dialog = function (buyer){

    this.id = buyer.name;
    this.buyer = buyer;
    this.state = "new";

    this.expert = undefined;

    this.messages = new Array();

    const requestCBAPI = () =>{

    };

    this.sendMessageToBuyer = (io, topic, message)=>{
    
        for (let s_id in io.sockets.sockets){
            let s = io.sockets.sockets[s_id]

            if (s.user !== undefined && s.user.name === this.buyer.name){
                s.emit(topic, '<strong>' + s.user.name + '</strong>: ' + message);
            }
        }
    };

    this.assignExpert = (expert) => {
        
        if (expert !== undefined && expert.role == "expert" && expert.name ) {
            this.expert = expert
        }
        else{
            throw Error("Wrong expert user type");
        }

    }

    this.revokeExpert = (expert) => {
        if (this.expert === undefined)
            throw Error("No expert is assigned to this dialog");

        if (expert !== undefined  && this.expert.name !== expert.name ){
            throw Error("Could not revoke unassigned expert");
        }

        if (expert !== undefined && this.expert.name === expert.name ) { 
            this.expert = undefined;
            return;}
    
    }


    this.getMessageRecipientsNames = () =>{
        let result = new Array();

        if (this.buyer !== undefined)
            result.push(this.buyer.name);

        if (this.expert !== undefined && this.expert.role === "expert" && this.expert.name  )
            result.push(this.expert.name);
        
        return result;
    }    
    /**
     * Add message into dialog amd it's histroy and post it via callback function
     * @param  {} content
     * @param  {} sender=undefined
     * @param  {Function} callback function (dialog, sender, message) to render added message 
     */
    this.postMessage = (content, sender = undefined, callback=undefined) => {
        let userFrom;
        if (sender === undefined) {
                 userFrom = {
                role: "console",
                name: "~$"
            }    
        }
        else{
                 userFrom = sender;
        }
         
        let usersToSend = this.getMessageRecipientsNames();

        if (["expert", "buyer", "bot", "console"].includes(userFrom.role)) 
            if (["text", "buttons", undefined].includes(content.type))
                this.messages.push(new Message(this, userFrom, content ));

        callback(this, userFrom, this.renderHtmlMessage(userFrom, content ));
        
    }

    this.renderHtmlMessage = (fromUser, content) =>{
        let text ="";

        if (content.type === undefined ) {
            text = content
        }
        else if (content.type === "text"){
            text = content.text;
        }
        else if (content.type === "buttons"){
            text = elements.renderButtons(content.title, content.buttons);
        }

        if (fromUser !== undefined) 
            //return `<b>${fromUser.name}:</b> ${text}`;
            return text;
        return "";
    }
    
    this.renderMessageHistory = () =>{
        let renderedMessages = new Array();

        for (let i in this.messages){
            let message = this.messages[i];

            renderedMessages.push({sender: message.sender,
                message:this.renderHtmlMessage(message.sender, message.content )});
        }
        return renderedMessages;
    }

    this.changeState = (newState,  callback) =>{
        let prevState = this.state;

        if (newState === this.state)
            throw Error ("Status not changed");
        
        this.state = newState; 
        callback(this, prevState, newState );
    }
}

function userSockets(io, ...users) {
    let sockets = new Array();

    let userNames = new Array();
    
    for (let user of users){
        if (user !== undefined) userNames.push(user.name);
    } 

    for (let s_id in io.sockets.sockets){
        let s = io.sockets.sockets[s_id]

        if (s.user !== undefined && userNames.includes(s.user.name)) 
         {
            sockets.push(s);
        }
    }

    return sockets;
                            
}


function sendMessageToUser(io,user, topic, message)  {
    
    for (let s_id in io.sockets.sockets){
        let s = io.sockets.sockets[s_id]

        if (s.user !== undefined && s.user.name === user.name){
            s.emit(topic,  message);
        }
    }


}

function findAssignedDialogsForExpert (allDialogs, expert){
    
    let  foundDialogs = new Array();
    
    for (let [dialogId, dialog] of Object.entries(allDialogs)) {
        if (dialog.expert !== undefined && dialog.expert.name === expert.name)
        foundDialogs.push(dialogId);
    } 

    return foundDialogs;

}


module.exports = Dialog;
module.exports.sendMessageToUser =  sendMessageToUser;
module.exports.findAssignedDialogsForExpert = findAssignedDialogsForExpert;
module.exports.Message = Message;
module.exports.userSockets = userSockets;
