const request = require('request');


/**
 * requests chat bot api and proceeds it with onRespose(responses) call back. responses is the list of cb-api content
 * elements .
 * @param botUlr
 * @param payload for cb-api request
 * @param onResponse  takes responses from bot platform as param
 * @param onRequestError callback with error. could be omitted
 */
exports.botRequest = (botUlr, payload, onResponse, onRequestError = undefined) =>
{
    typing = true;
    let req_body;

    if (typeof (payload) === "string")
        req_body = payload;
    else
        req_body = JSON.stringify(payload);

    console.log('Requesting cbapi...: ' + req_body);

    request.post({
        uri: botUlr,
        body: req_body,
        headers: {'content-type': 'application/json'}
    }, function (error, response, body) {

        typing = false;
        console.error('error:', error);
        console.log('statusCode:', response && response.statusCode);
        console.log('body:', body);

        try {

        if (error == null && body != null) {
            let parsed_message = JSON.parse(body);
            let responses = parsed_message.responses;
            onResponse(responses);
        }
        else
        {
            if (typeof onRequestError === 'function') {
                onRequestError(error);
            }
        }}
        catch(e){
            onRequestError(e);
        }


    })
};
