/*
usage sample
function print(a,b,c){

    console.log(a,b,c);
 }


let cmd = " /print 3 4 5 ";

if  
(runChatCommand(cmd, 
    {"print arg1 arg2 arg3 | prints provided 3 arguments": (arg1, arg2, arg3) => { print(arg1, arg2, arg3) },
    "cls":(args) => console.log("NONE"),
    "help": (message) => console.log(message)  }     
 ))
 {
     console.log("Executed chat command");
 }
else {console.log("No command found"); }
*/

// function runChatCommand(message, bindedCommand){

//     return new Promise((resolve, reject) => {

//         let prepared_message = message.trim().replace(/\s\s+/g, ' ');
    
//         if (prepared_message[0] !== "/")
//             reject("Fail");
        
//         let entities = prepared_message.split(" ");
    
//         let cmd = entities[0];
//         let args = entities.slice(1);

//         if (cmd === "/"+bindedCommand){

//             resolve(args);
//         }
//     })
    
// }

function runChatCommand(cmd, bindings, displayCommand = undefined, displayError = undefined) {

    let prepared_message = cmd.trim().replace(/\s\s+/g, ' ');

        

        if (prepared_message[0] !== "/")
            return false;
        else {
            
            if (displayCommand !== undefined)
                displayCommand(cmd); 
        }

        let entities = prepared_message.split(" ");

        let extractedCmd = entities[0].slice(1);
        let args = entities.slice(1);
        
        if (extractedCmd === 'help'){
            var message = Array();
            
            for (let [cmd, func] of Object.entries(bindings)){
                    message.push("/"+cmd);
            }

    
            bindings["help"].apply("",[message]);
            return true;
        }

        let commands = {}; 
        for (let [cmd, func] of Object.entries(bindings)){
            commands[cmd.trim().replace(/\s\s+/g, ' ').split(" ")[0]] = func
        }
        

        if (commands[extractedCmd] !== undefined)
           {commands[extractedCmd].apply("", args);
           return true;}
        else if (prepared_message[0] === "/")
            {
                if (displayError === undefined)
                    displayCommand("Not found. Use /help for help")
                else
                    displayError("Command not found. Use <b>/help</b> for help")
                return true;}
        else
            return false;

}

module.exports = runChatCommand;
