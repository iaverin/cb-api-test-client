var assert = require('assert');
var expect = require('chai').expect

var dialogs = require("../dialogs");
var user = require("../user");

describe('Working with dialogs', function() {
    
    let buyer1 = new user(user.type["buyer"], "buyer1");
    let buyer2 = new user(user.type["buyer"], "buyer2");

    let expert1 = new user(user.type["expert"], "expert1");
    let expert2 = new user(user.type["expert"], "expert2");
    
    let dialog1 = new dialogs(buyer1);
    let dialog2 = new dialogs(buyer2);

    let allDialogs = {};

    allDialogs[dialog1.buyer.name] = dialog1;
    allDialogs[dialog2.buyer.name] = dialog2;

    it("dialogs should be created for buyer1 and buyer2", function(){

        expect(allDialogs[dialog1.buyer.name].buyer).to.be.eql(buyer1);
        expect(allDialogs[dialog2.buyer.name].buyer).to.be.eql(buyer2);

    });

    it("should assign dialog1 to expert1",function(){
        dialog1.assignExpert(expert1);
        expect(allDialogs[dialog1.buyer.name].expert).to.be.eql(expert1);
    });

    it("should find that expert1 is assigned to dialog1", function(){
        expect(dialogs.findAssignedDialogsForExpert(allDialogs, expert1)).to.be.eql([dialog1.buyer.name]);
    })
    
    it("should also assign expert1 to dialog2 and found it also assigned", function(){
        dialog2.assignExpert(expert1);
        expect(dialogs.findAssignedDialogsForExpert(allDialogs, expert1)).to.be.eql([dialog1.buyer.name,
             dialog2.buyer.name]);
    });

    it("should be able to revoke assignment from dialog2", function(){
        dialog2.revokeExpert(expert1);
        expect(dialogs.findAssignedDialogsForExpert(allDialogs, expert1)).to.be.eql([dialog1.buyer.name]);
        expect(dialog2.expert).to.be.undefined;
    })

    it("should fire an error when revokes non assigned expert", function(){
        expect(()=>{dialog1.revokeExpert(expert2) }).to.throw("Could not revoke unassigned expert");
    });

    it("should fire an error when revokes dialog from dialog which has not assigned expert", function(){
        expect(()=>{dialog2.revokeExpert(expert1) }).to.throw("No expert is assigned to this dialog");
    });


  


})
