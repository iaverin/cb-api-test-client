var assert = require('assert');
var expect = require('chai').expect;

user = require("../user");

describe('ChatBit-API Users', function() {

    it('should include attribute buyer withe the value 0', function(){
        expect( user.type.buyer ).to.equal(0);
    });

    it('should include attribute buyer withe the value 0', function(){
        expect( user.type.bot ).to.equal(2);
    });

    it('should return 1 for type of user.expert', function () {
       expect(user.type["expert"]).to.equal(3);
    });

    it('should return "robot" for created user with name "robot"  ', function () {
        expect(
            new user(1, "robot").name
        ).to.equal("robot")

    });

    it("should fire up the exception if wrong text role was provided", function() {
        expect(user["test"]).to.be.equal(undefined);
    });


})
