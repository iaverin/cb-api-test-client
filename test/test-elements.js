var assert = require('assert');
var expect = require('chai').expect

var elements = require("../elements");

describe('Rendering buttons', function() {

    const buttonsHtml = elements.renderButtons("Buttons for test",[{
        title:"Button 1", payload:"btn1"},
        {title:"Button 2", payload:"btn2"}
    ]); 

    it('should include text with title', function(){
        expect( buttonsHtml ).to.include("Buttons for test");
    })

    it('should include captions for buttons', function(){
        expect( buttonsHtml ).to.include("Button 1");
        expect( buttonsHtml ).to.include("Button 2");
    })

    it('should include payloads', function(){
        expect( buttonsHtml ).to.include("btn1");
        expect( buttonsHtml ).to.include("btn2");
    })

  });
  

