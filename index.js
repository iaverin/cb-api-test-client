const express = require('express');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);

const elements = require('./core/elements');
const User  = require("./core/user");
const dialogs = require("./core/dialogs");
const runChatCommand = require("./core/chatcommand");

const requiredConfigParams = ["CHATBOTAPI_BASE_URL", "BACKEND_URL"];
const EXPERT_USER_NAME_PREFIX = "expert";

const SmartbeSocket = require("./smartbe-socket");

const client = require("./core/client");
const cbapi = require("./core/bot");

// loading config and check required params
try {
    var config = require("./.config");

    for( let configParam of requiredConfigParams ){
       if (eval( "config."+configParam) === undefined )
       {
           throw {message: "No " + configParam + " in config. Check config_def.js for required parameters!"}

        }
    }
}
catch(e){
    if (e.code ===  'MODULE_NOT_FOUND') {

    console.log("No config found! Rename config_def.js to .config.js");
    }

    else{
    console.log(e.message);
    }
    process.exit();

}


const  CHATBOTAPI_BASE_URL = config.CHATBOTAPI_BASE_URL;
const chatBotRequestUrl = CHATBOTAPI_BASE_URL + "requestBot";
const BACKEND_URL = config.BACKEND_URL;

const BOT_USER = new User(User.type["bot"], "BOT" );
const CONSOLE = undefined;

console.log("-------------------");
console.log();
console.log("Config:");
console.log("BackEnd URL: " + BACKEND_URL);
console.log("ChatBotApi URL: "+CHATBOTAPI_BASE_URL);
console.log("-------------------");

const allDialogs = new Object();

// config static files directory
app.use('/static', express.static(__dirname + '/static'));


// start the app
app.get('/', function(req, res) {
    data = {
        backEndUrl : BACKEND_URL
    };
    res.render('index.ejs', data);
});


io.sockets.on('connection', function(socket) {

    function onSendTyping(dialog){
        client.userSockets(io.sockets.sockets, dialog.buyer, dialog.expert).forEach(socket => {
            if (socket.dialog !== undefined && socket.dialog.id === dialog.id)
                socket.emit("typing", "");})
    }

    async function botRequest(payload) {

        typing = true;
        console.log('Requesting cbapi...: ' + payload);

            cbapi.botRequest(chatBotRequestUrl, payload, (responses) => {
                client.parseResponses(io.sockets.sockets, socket.dialog, responses, BOT_USER, onSendTyping,
                    socket.smartbeSocket);
            },  (error) => {
                client.userSockets(io.sockets.sockets, socket.user).forEach((socket) => {
                    socket.emit('chat_message', '<strong>ERROR:</strong> ' + error)
                });
                console.log("Could not parse response");
            });

        typing = false;
    }

    function systemRequest(payload, responseCallback){
        console.log('Requesting cbapi...: ' + payload);
        cbapi.botRequest(CHATBOTAPI_BASE_URL+'system', payload, (responses) => {
            console.log(responses)
            if(!responses[0].error) {
                responseCallback(responses[0])
                return
            }

            client.userSockets(io.sockets.sockets, socket.user).forEach((socket) => {
                socket.emit('chat_message', '<strong>ERROR:</strong> ' + responses.error)
            })

        },
            (error)=>{
                client.userSockets(io.sockets.sockets, socket.user).forEach((socket) => {
                    socket.emit('chat_message', '<strong>ERROR:</strong> ' + error)
                })

            })

    }

    function initUser(userinfo, forceProfile = undefined){
        let role = "buyer";

        if (userinfo.isExpert === true)
            role = "expert";
        let dialogProfile = {};

        if (forceProfile !== undefined)
            dialogProfile = forceProfile;


        socket.user = new User( User.type[role],  userinfo.username);
        socket.activeDialogId  = undefined;  // active dialog for "window"


        socket.emit("chat_message", '----------------------------------------------------------------------------');
        socket.emit("chat_message", "<b>~$</b>: Current user <b>"+ socket.user.name + "</b> connected as <b>" + socket.user.role+'</b');

        if (socket.user.role === "buyer") {
            let payload;

            socket.activeDialogId = socket.user.name;

            if (allDialogs[socket.user.name] === undefined)  {
                socket.dialog = new dialogs(socket.user);
                allDialogs[socket.user.name] = socket.dialog;
                socket.emit('chat_message', `<b>~$</b>: New dialog created for user ${socket.user.name}`);

                if (dialogProfile. visit === undefined)
                    dialogProfile.visit = "initial";

                payload ={
                    userId:  socket.user.name,
                    type: "init",
                    state: socket.dialog.state, // new
                    profile: dialogProfile   // {"visit": "initial"}
                };

                socket.profile = payload.profile;

            }
            else{
                socket.dialog =  allDialogs[socket.user.name];

                if (dialogProfile.visit === undefined)
                    dialogProfile.visit = "next";

                payload ={
                    userId:  socket.user.name,
                    type: "init",
                    state: allDialogs[socket.user.name].state,
                    profile: dialogProfile
                };

                socket.profile = payload.profile;

                socket.emit('chat_message', `<b>~$</b>: Dialog exists:  ${socket.user.name}`);

            }
            socket.emit('chat_message', `<b>~$</b>: Dialog state:  ${socket.dialog.state}`);
            socket.emit('chat_message', `<b>~$</b>: Profile:  ${JSON.stringify(socket.profile, " ")}`);

            botRequest(payload);

        }
        if (socket.user.role === "expert") {
            socket.emit('chat_message', `<b>~$</b>: Expert ${socket.user.name} is assigned to dialogs: ${ dialogs.findAssignedDialogsForExpert(allDialogs, socket.user) }`);
            socket.dialog = undefined;
        }

        socket.emit("chat_message", "<b>~$</b>: send /help for commands list.");
        socket.emit("chat_message", '----------------------------------------------------------------------------');

        // display history
        if (socket.dialog !== undefined && socket.dialog.messages.length >0 ){
            socket.dialog.renderMessageHistory().forEach( (message) =>{
                socket.emit("chat_message", `<b>${message.sender.name}</b>: ${message.message}`);
            } );
        }

    }

    function proceedChatCommand(message){
        return runChatCommand(message,
            {
                "help":  (commands) => {
                    socket.emit( 'chat_message',
                        "<strong>~$</strong>: List of commands: <br/><p style='padding-left:15px'> " + commands.join ("<br/>") + '</p>');
                },

                "take user_name : assign user_name to current expert" : (userName) => {

                    if (allDialogs[userName].state !== "expert")
                        throw("Dialog is not in EXPERT state");

                    //clean dialog from other's experts socket
                    client.userSockets(io.sockets.sockets, allDialogs[userName].expert ).forEach( (socket) => {
                        if (socket.user.role === "expert" && socket.user.name === allDialogs[userName].expert.name  )
                        { socket.dialog = undefined; }
                    })

                    allDialogs[userName].assignExpert(socket.user);
                    socket.dialog = allDialogs[userName];

                    socket.dialog.postMessage('Dialog ' + socket.dialog.id +
                        ' taken by ' + socket.user.name, CONSOLE,  (dialog, sender, message) =>
                    {client.sendToClientsInDialog(io.sockets.sockets, dialog, sender, message)});

                },

                "dialog [username] : shows current dialog and all taken. or switch to dialog with username ": (userName)=>{
                    if (userName === undefined && socket.user.role === "expert") {

                        let activeDialogId = socket.dialog === undefined ? "None" : socket.dialog.id;

                        socket.emit('chat_message','<strong>~$</strong>: ' +
                            `Active dialog ${activeDialogId}. Assigned to dialogs:  ${dialogs.findAssignedDialogsForExpert(allDialogs, socket.user)}`);
                        return;
                    }

                    if (socket.user.role === "expert" && allDialogs[userName].expert.name === socket.user.name) {
                        socket.dialog = allDialogs[userName];
                        socket.emit('chat_message','<strong>~$</strong>: ' +
                            'Switched to dialog: ' + userName);
                        //todo: load history
                    }
                    else{
                        throw Error("Could not switch to dialog " + userName );
                    }
                },

                "drop : leave the active dialog": ()=>{
                    socket.dialog.revokeExpert(socket.user);
                    let leftDialogId = socket.dialog.id
                    socket.dialog = undefined;

                    socket.emit('chat_message','<strong>~$</strong>: ' +
                        'Has left the dialog with:' + leftDialogId);
                },

                "history : show dialog history" : () => {
                    if (socket.dialog === undefined )
                        throw("No open dialog")

                    socket.emit("chat_message", "------------------------------------------") ;
                    socket.dialog.renderMessageHistory().forEach( (message) =>{
                        socket.emit("chat_message", `<b>${message.sender.name}</b>: ${message.message}`);
                    } );

                },

                "state [to_state] : current dialog state.  [to_state] updates state (new, expert, bot)." : (state) => {
                    if (state !== undefined)
                        socket.dialog.changeState(state,  (dialog, prevState, newState ) => {client.notificationDialogState(io.sockets.sockets,
                            dialog, prevState, newState)})
                    else
                        socket.emit('chat_message','<strong>~$</strong>: ' +
                            'Current state: ' + socket.dialog.state);

                },

                "bot : sets the bot status to current dialog": () =>{
                    socket.dialog.changeState("bot",  (dialog, prevState, newState ) => {client.notificationDialogState(io.sockets.sockets,
                        dialog, prevState, newState)});
                },

                "profile [attribute] [value] :  dialog's profile. Use [attribute] [value] to set.": (attribute, value) =>{
                    if (socket.profile === undefined)
                        socket.profile = {};

                    socket.profile[attribute] = value;
                    socket.emit('chat_message','<strong>~$</strong>: ' +
                        `The profile: ${JSON.stringify(socket.profile, " ")}`);
                },

                "reload : reload's the dialog using state and profile set by /status and /profile" : () =>{
                    let userInfo = {};
                    userInfo.isExpert = socket.user.role === "expert" ? true : false;
                    userInfo.username = socket.user.name;
                    socket.emit("cls", "");
                    initUser(userInfo, socket.profile);
                    socket.profile = undefined;
                },

                "reset : clear the dialog and proceed with the /profile. state=new and profile.visit=initial " : () =>{
                    let userInfo = {};
                    userInfo.isExpert = socket.user.role === "expert" ? true : false;
                    userInfo.username = socket.user.name;
                    delete allDialogs[socket.dialog.buyer.name];

                    // socket.profile["visit"] = "initial";
                    socket.emit("cls", "");
                    socket.profile.visit = "initial";
                    initUser(userInfo, socket.profile);
                },

                "resetUser : reset user in errors database":()=>{
                     systemRequest({
                        command:"resetUser",
                        userId: socket.user.name
                    }, (response) => {
                         socket.emit('chat_message','<strong>~$:</strong> resetUser'+JSON.stringify(response))
                     })
                },

                "missed_dialog : test missed dialog flow. Should be called in <b>expert</b> state ": () => {
                    if (socket.dialog.state !== "expert") {
                        throw("Should be called in <b>expert</b> state");
                    }
                    // connect to cb-api-client socket
                    let extSocket = require('socket.io-client').connect("http://localhost:8080");
                    extSocket.on('connect', () => {
                        console.log("External connected");
                        client.userSockets(io.sockets.sockets, socket.dialog.buyer).forEach((socket) => {
                            socket.emit('chat_message', 'Requesting missed dialog flow...');
                        });

                        extSocket.emit("system_message", {
                            type: "call_bot", dialogId: socket.dialog.id,
                            profile: {
                                not_taken: "yes"
                            },
                        })
                    })
                },
                "tutorial : shows tutorial (botpress flow tutorial)" : () =>{
                    let help_req ={
                    userId:  socket.user.name,
                    type: "init",
                    state: "",
                    profile: {"flow":"tutorial"}
                };
                    botRequest(help_req);}

            }, (cmd) => { socket.emit('chat_message','<strong>'+socket.user.name+'</strong>: '+cmd); },
            (cmd) => { socket.emit('chat_message','<strong>~$:</strong> '+cmd); })

    }


    socket.on('username', function(userinfo) {
        initUser(userinfo);

        // init the smartbe server emulator - buyer chat
        socket.smartbeSocket = new SmartbeSocket('http://localhost:3000',  (content, sender) => {
            socket.dialog.postMessage(content, sender, (dialog, sender, message) =>{
                client.sendToClientsInDialog(io.sockets.sockets, dialog, sender, message)})
        });

    });

    socket.on('disconnect', function(userinfo) {

        let username = "";

        if (this.user !== undefined)
            username = this.user.name;

        //io.emit('is_online', '🔴 <i>' + username + ' left the chat..</i>');
        console.log(username +":socket disconnected" );
    })

    var typing =  false;

    socket.on('chat_message', function(message) {

        console.log('Received message: '+message.substr(0, 50) + "...");

        //
        // proceed chat commands
        //
        try {

            if ( proceedChatCommand(message) )

                return ; //will not proceed with command message further
        }
        catch (e) {
            socket.emit('chat_message','<strong>~$</strong>: '+message);
            socket.emit('chat_message','Error proceeding command!<br/>'+e);
            return ;
        }

        //
        // if there is no dialog than skip the message (in case of expert)
        //
        if (socket.dialog === undefined ) {
            socket.emit('chat_message','No dialog assigned');
            return;
        }

        //
        // adding  message to dialog
        //
        socket.dialog.postMessage(message, socket.user, (dialog, sender, message) =>
            {client.sendToClientsInDialog(io.sockets.sockets, dialog, sender, message, socket.smartbeSocket)});

            //
            // call bot
            //
            if (socket.user.role === "buyer" && socket.dialog  && typing === false) {

                let req_body = JSON.stringify(
                    {
                        userId: socket.user.name,
                        type: "message",
                        message: message
                    },
                )
                botRequest(req_body);
            }
    })


    ///
    // proceed system messages from chat-api (expert chat)
    ///
    socket.on('system_message', function(message) {

        if (!["system", "call_bot"].includes(message.type)){
            console.log('Not a System message message on system message socket' + JSON.stringify(message));
            return;
        }


        let dialogId = message.dialogId;
        let dialog = allDialogs[dialogId];

        console.log('System message message: '+ JSON.stringify(message));

        // find socket with the dialog_id
        // extract requests

      if (message.type === "call_bot" && message.profile !== undefined ) {
          let payload = {
              userId: dialog.buyer.name,
              type: "init",
              state: dialog.state,
              profile: message.profile
          };
              cbapi.botRequest(chatBotRequestUrl, payload, (responses) => {
                  client.parseResponses(io.sockets.sockets, dialog, responses, BOT_USER, onSendTyping).catch(
                      (reason) => {console.log(reason)}
                  );
              }, (error) => {
              console.log("Request error: " + error);
          });
      }
    })
});

const server = http.listen(8080, function() {
    console.log('listening on *:8080');
});

